import { Component, ViewChild } from '@angular/core';

import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  noticias: any = [];
  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.loadNticias();
  }

  loadData(event) {
    console.log(event);
    this.loadNticias(event);
  }
  loadNticias(event?) {
    this.dataService.nextPage();
    this.dataService.getNoticias().subscribe((data) => {
      this.noticias.push(...data.articles);
      if (event) {
        event.target.complete();
      }
      //console.log(data);
    });
  }
}
