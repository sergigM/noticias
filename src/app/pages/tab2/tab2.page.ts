import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  categorias = [
    'business',
    'entertainment',
    'general',
    'health',
    'science',
    'sports',
    'technology',
  ];
  noticias: any = [];

  segmentChanged(ev: any) {
    this.dataService.resetPage();
    this.dataService.setCategoria(ev.detail.value);
    this.noticias = [];
    this.loadNticias();
    console.log('Segment changed', ev.detail.value);
  }
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.loadNticias();
  }

  loadData(event) {
    this.loadNticias(event);
  }
  loadNticias(event?) {
    this.dataService.nextPage();
    this.dataService.getNoticias().subscribe((data) => {
      this.noticias.push(...data.articles);
      if (event) {
        event.target.complete();
      }
      console.log(data);
    });
  }
}
