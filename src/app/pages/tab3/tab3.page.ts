import { Component } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service';
import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  constructor(private dataServiceLocal: DataLocalService) {}
  noticias: Article[] = [];
  ngOnInit() {
    this.cargar();
    
  }
  async cargar() {
    const a = await this.dataServiceLocal.cargarrFav();
    this.noticias = a;
  }
  ionViewWillEnter(){
    this.cargar();
  }
}
