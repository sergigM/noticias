import { NgModule } from '@angular/core';
import { NoticiasComponent } from './noticias/noticias.component';

@NgModule({
  declarations: [NoticiasComponent],
  exports: [NoticiasComponent],
})
export class ComponentsModule {}
