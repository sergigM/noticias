import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

import { Browser } from '@capacitor/core';
import { Article } from 'src/app/interfaces/interfaces';
import { DataLocalService } from '../../services/data-local.service';
import { Plugins } from '@capacitor/core';
const { Share } = Plugins;

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss'],
})
export class NoticiasComponent implements OnInit {
  category: string = 'general';

  @Input() noticias: Article[] = [];

  constructor(
    public actionSheetController: ActionSheetController,
    private dataServiceLocal: DataLocalService
  ) {}

  ngOnInit() {}
  async lanzarMenu(noticia) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Share',
          icon: 'share',
          handler: async () => {
            await Share.share({
              title: 'See cool stuff',
              text: 'Really awesome thing you need to see right meow',
              url: noticia.url,
              dialogTitle: 'Share with buddies'
            });
            console.log('Share clicked');
          },
        },
        {
          text: 'Favorite',
          icon: 'heart',
          handler: () => {
            this.dataServiceLocal.guardarNoticia(noticia);
            console.log('Favorite clicked');
          },
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
  
  

  async openNavigator(a) {
    // On iOS, for example, open the URL in SFSafariViewController (the in-app browser)
    await Browser.open({ url: a });
  }
}
