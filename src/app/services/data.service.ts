import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Noticia } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}
  page: number = 0;
  categoria: string = 'general';

  getNoticias() {
    return this.http.get<Noticia>(
      `https://newsapi.org/v2/top-headlines?language=es&page=${this.page}&category=${this.categoria}&apiKey=${environment.apiKey}`
    );
  }
  setCategoria(newCategory) {
    this.categoria = newCategory;
    console.log(this.categoria);
  }
  resetPage() {
    this.page = 0;
  }
  nextPage() {
    this.page++;
  }
}
