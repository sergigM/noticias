import { Injectable } from '@angular/core';
import { Article, Noticia } from '../interfaces/interfaces';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root',
})
export class DataLocalService {
  noticias: Article[] = [];
  constructor(private storage: Storage) {
    this.storage.create();
    this.cargarrFav();
  }
  guardarNoticia(noticia: Article) {
    this.noticias.unshift(noticia);
    this.storage.set('favoritos', this.noticias);
  }
  async cargarrFav() {
    const favoritos = await this.storage.get('favoritos');
    if (favoritos) {
      this.noticias = favoritos;
      return this.noticias;
    }
  }
}
